# raccoon-llc-test

Hello :)

Please, find additional information below.
Plese, don't use UI frameworks here. Please, use only html, css/scss, js.

1. Design link: https://www.figma.com/file/wHd8CYFAXNvKNMRZhKld8h/CoffeeStyle?node-id=0%3A1
2. Only part from the Design.png should be done.
3. Desktop version should be according to the design
4. For the mobile version (up to 1023px, 1024px - use design for the desktop), please, 
   show 1 Big product tile per row and 2 small product tiles per row.
   Please, see Design_mobile.png as a reference

Additional Task (optional)
1. Please, see the part from the Additional.png as a reference for Desktop
2. For the mobile, please, show blocks with images in one row and
   Info section below images (see Additional_mobile.png)

Additional Task 2 (optional)
1. Please, add images carousel for a product tile. Any js framework can be used here.

Thank you!
